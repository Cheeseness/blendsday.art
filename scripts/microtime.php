<?php
	header('Content-Type: application/javascript');
	header("Expires: Thu, 23 Mar 1972 07:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
?>
var serverTime = <?php echo microtime(true); ?>;
