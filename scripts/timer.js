/**
 * Event timer (and assorted little things) for Double Fine Game Club, adapted for Blendsday
 * Copyright � Janne Enberg 2012
 * Copyright � Josh "Cheeseness" Bush 2023
 * Distributed with the new BSD license and MIT licenses below, which basically means:
 * "By all means, take it, but don't blame me if it breaks."
 *
 * The "New" BSD License:
 * ----------------------
 * 
 * Copyright (c) 2012, Janne Enberg
 * Copyright (c) 2023, Josh "Cheeseness" Bush
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *   * Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *   * Neither the name of Janne Enberg nor the names of its contributors
 *     may be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * 
 * MIT License
 * -----------
 *
 * Copyright (c) 2012, Janne Enberg
 * Copyright (c) 2023, Josh "Cheeseness" Bush
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */
(function(){		
// Event configuration
//var eventDOW = 6; // Day of week .. 0-6, where 0 = Sun, 6 = Sat (Saturday)
var eventBase = 60 * 60 * 1000; // UTC
var eventTime = eventBase * eventOffset; //2300UTC
var eventLength = 1.5 * 60 * 60 * 1000; // Approximate event length in msec (1.5hrs)
var dayArray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var forceUpdatePrompt = false;


var log = function() {
	if (console && console.log && console.log.apply) {
		if (arguments.length === 1) {
			console.log(arguments[0]);
		} else {
			console.log(Array.prototype.slice.call(arguments));
		}
	}
}

var updateEventTime = function() {
	eventTime = eventBase * eventOffset;
}

var initEventHandlers = function() {
	var requiredElements = [
		"getRandomPrompt",
		"getRandomActivity",
		"randomPromptCount",
		"randomPrompts",
		"customTime",
		"customDay",
		"permalink",
		"customOptions",
		"enableActivities",
		"shade",
	];
	
	for (var i = 0, count = requiredElements.length; i < count; ++i)
	{
		if (!document.getElementById(requiredElements[i]))
		{
			return false;
		}
	}

	var promptCount = document.getElementById("randomPromptCount");
	var promptContainer = document.getElementById("randomPrompts");
	var activityContainer = document.getElementById("randomActivity");

	document.getElementById('getRandomPrompt').onclick = function(event) {
		var count = promptCount.value;
		promptContainer.innerHTML = "";
		if (prompts.length >= count)
		{
			var tempList = prompts.slice(0);
			for (var i = 1; i < tempList.length; i++)
			{
				var j = Math.floor(Math.random() * (i - 1));
				var temp = tempList[i];
				tempList[i] = tempList[j]
				tempList[j] = temp;
			}
			while (count > 0)
			{
				var p = document.createElement("span");
				p.innerHTML = tempList[count - 1];
				promptContainer.appendChild(p);
				promptContainer.appendChild(document.createTextNode(" "));
				count --;
			}
		}
		else
		{
			var notice = document.createElement("p");
			notice.className = "notice";
			notice.innerHTML = "Prompt list is too short to accommodate request.";
			promptContainer.appendChild(notice);
			for (var i = 0; i < prompts.length; i++)
			{
				var p = document.createElement("span");
				p.innerHTML = prompts[i];
				promptContainer.appendChild(p);
				promptContainer.appendChild(document.createTextNode(" "));
			}
		}
	}

	document.getElementById('getRandomActivity').onclick = function(event) {
		activityContainer.innerHTML = "";

		if (activities.length > 0)
		{
			var a = activities[Math.floor(Math.random() * activities.length)].split(/\ -\ (.*)/s);
			var p = document.createElement("p");
			p.className = "activityName";
			p.innerHTML = a[0];
			activityContainer.appendChild(p);
			if (a.length > 1)
			{
				p.innerHTML += "<br /><em>" + a[1] + "</em>";
				p = document.createElement("br");
				eventPrompt.appendChild(p);
			}
		}
		else
		{
			var notice = document.createElement("p");
			notice.innerHTML = "Activities list is too short to accommodate request.";
			notice.className = "notice";
			activityContainer.appendChild(notice);
		}
	}



	var customTime = document.getElementById('customTime');
	var customDay = document.getElementById('customDay');
	var permalink = document.getElementById("permalink");

	var customPrompts = document.getElementById("customPrompts");
	var customActivityFrequency = document.getElementById("customActivityFrequency");
	var customActivityOffset = document.getElementById("customActivityOffset");
	var customActivities = document.getElementById("customActivities");

	for (var i = 0; i < 24; i++)
	{
		var option = document.createElement("option")
		option.id = i;
		option.value = i;
		option.innerHTML = ("" + i).padStart(2, "0") + ":00";
		customTime.appendChild(option);
	}
	customTime.value = eventOffset;

	for (var i = 0; i < 7; i++)
	{
		var option = document.createElement("option")
		option.id = i;
		option.value = i;
		option.innerHTML = dayArray[i];
		customDay.appendChild(option);
	}
	customDay.value = eventDOW;



	document.getElementById("customOptions").checked = false;
	document.getElementById("enableActivities").checked = false;

	if (promptURI != defaultPromptURI)
	{
		customPrompts.value = promptURI;
		document.getElementById("customOptions").checked = true;
		document.getElementById("customMore").classList.remove("hiddenForm");
	}
	if (activityURI != defaultActivityURI)
	{
		customActivities.value = activityURI;
		document.getElementById("customOptions").checked = true;
		document.getElementById("customMore").classList.remove("hiddenForm");
		document.getElementById("enableActivities").checked = true;
		document.getElementById("customActivitiesMore").classList.remove("hiddenForm");
	}
	if (activityFrequency != defaultActivityFrequency)
	{
		customActivityFrequency.value = activityFrequency;
		document.getElementById("customOptions").checked = true;
		document.getElementById("customMore").classList.remove("hiddenForm");
		document.getElementById("enableActivities").checked = true;
		document.getElementById("customActivitiesMore").classList.remove("hiddenForm");
	}
	if (activityOffset != defaultActivityOffset)
	{
		customActivityOffset.value = activityOffset;
		document.getElementById("customOptions").checked = true;
		document.getElementById("customMore").classList.remove("hiddenForm");
		document.getElementById("enableActivities").checked = true;
		document.getElementById("customActivitiesMore").classList.remove("hiddenForm");
	}

	document.getElementById('setCustomTime').onclick = function(event){
		eventDOW = customDay.value;
		eventOffset = customTime.value;

		var url = ""
		if (eventDOW != defaultEventDOW)
		{
			url += "&d=" + eventDOW;
		}
		if (eventOffset != defaultEventOffset)
		{
		 	url += "&t=" + eventOffset;
		}
		if (document.getElementById("customOptions").checked === true)
		{
			if (customPrompts.value != "")
			{
				url += "&prompts=" + customPrompts.value;
			}
		}

		if (document.getElementById("enableActivities").checked === true)
		{
			url += "&a=" + customActivityFrequency.value;
			activityFrequency = parseInt(customActivityFrequency.value);
			if (customActivityOffset.value != 0)
			{
				url += "&ao=" + customActivityOffset.value;
				activityOffset = parseInt(customActivityOffset.value);
			}
			if (customActivities.value != "")
			{
				url += "&activities=" + customActivities.value;
				activitiesURI = customActivities.value;
				//TODO: Reload activities list?
			}
		}

		if (url != "")
		{
			url = "?" + url.substr(1);
		}
		else
		{
			url = location.protocol + '//' + location.host + location.pathname;
		}

		forceUpdatePrompt = true;
		window.history.pushState({day: eventDOW, time: eventOffset}, "Blendsday - Practice Blender!", url);
		permalink.innerHTML = "";
		var link = document.createElement("a");
		link.href = window.location.href;
		link.innerHTML = "Permalink to this new time";
		permalink.appendChild(link);
		currentWeek = null;
	};

	document.getElementById("customOptions").onclick = function(event){
		var form = document.getElementById("customMore");
		if (this.checked === true)
		{
			form.classList.remove("hiddenForm");
		}
		else
		{
			form.className += " hiddenForm";
		}
	};

	document.getElementById("enableActivities").onclick = function(event){
		var form = document.getElementById("customActivitiesMore");
		if (this.checked === true)
		{
			form.classList.remove("hiddenForm");
		}
		else
		{
			form.className += " hiddenForm";
		}
	};

	document.getElementById("shade").onclick = function(event){
		var shade = this;
		shade.style.opacity = 0;
		document.getElementById("previewCaption").style.opacity = 0;
		setTimeout(function() { shade.style.display = "none";}, 250);
	};

	return true;
};

var initTimer = function() {
	// Timer update logic
	var updateTimer = (function() {

		// By default we don't know the server time diff
		var serverTimeDiff = 0;

		// Cache the countdown element
		var countdownElement = document.getElementById('countdownText');
		var countdownElement2 = document.getElementById('countdownText2');

		// Cache the timeanddate.com link element
		var timeanddateElement = document.getElementById('timeanddateLink');

		// Cache the second timeanddate.com link element
		var timeanddateElement2 = document.getElementById('timeanddateLink2');
		
		// Cache the time indicator element
		var timeElement = document.getElementById('timeIndicator');
		
		// Check that it actually exists, abort if not
		if (!countdownElement) {
			return false;
		}

		// Make current time accesible for all the functions in this scope
		var currentTime = null;
		
		var checkServerTime = function() {
			// If we have server time, calculate the difference to it
			if (serverTimeDiff === 0 && typeof serverTime !== 'undefined') {
				serverTimeDiff = (typeof serverTime !== "undefined"? (new Date().getTime()) - Math.floor(serverTime * 1000) : 0);
				log('Found server time, diff now: ' + serverTimeDiff + ' msec');
			}
		};

		// Get the time of the next event
		var getNextEventTime = function() {
		
			// Get a new date for the current time
			var date = new Date();
			
			// Calculate difference in weekdays
			var dowDiff = date.getUTCDay() - eventDOW;
			
			// If not on the event DOW, update day
			if (dowDiff !== 0) {
			
				// Update the day of week				
				date = new Date(date.getTime() + ((0-dowDiff) * 24 * 60 * 60 * 1000))
				
			}
			
			// Reset to midnight
			date.setUTCHours(0);
			date.setUTCMinutes(0);
			date.setUTCSeconds(0);
			date.setUTCMilliseconds(0);

			// And fix the date to the time of the event
			updateEventTime();
			date = new Date(date.getTime() + eventTime);
			
			// But if we've passed the event, add a week
			if (currentTime > date.getTime() + eventLength) {
				date = new Date(date.getTime() + (7 * 24 * 60 * 60 * 1000))
			}
			
			//log('Next event  : ' + date);
							
			return date.getTime();
		};
		
		// Function to format milliseconds to [D days, ]HH:MM:SS text
		var formatMsec = (function() {
		
			// Function to zero-pad a number
			var zeroPad = function(number) {
				return (number>=10 ? number : '0'+number);
			};
			
			// Function with the core format logic
			return function(msec) {
			
				// Units we want to calculate
				var unitList = {
					days: 24 * 60 * 60 * 1000,
					hours: 60 * 60 * 1000,
					minutes: 60 * 1000,
					seconds: 1000
				};
				
				// Where to store the result
				var result = {};
				
				// Loop through the units
				for (var unit in unitList) {
					// Get the value off the list
					var value = unitList[unit];

					// By default, it doesn't fit
					var count = 0;
					
					// If it does however fit
					if (msec > value) {
					
						// Update how many of these units is in it
						count = Math.floor(msec / value);
						
						// And take off from the remainder
						msec -= count * value;
					}
					
					// Save result for this unit
					result[unit] = count;
				}
				
				
				// If there are days remaining, start with the number of days
				var text = (result.days > 0 ? result.days + ' days, ' : '');
				
				// Append the HH:MM:SS
				text += zeroPad(result.hours) + ':' + zeroPad(result.minutes) + ':' + zeroPad(result.seconds);
				
				// Give back the formatted text
				return text;
			};
		})();

		// What to do on each tick of the clock
		var tick = function() {
		
			// Check if we have server time and calculate diff if we do
			checkServerTime();
		
			// Get the current time (unix timestamp in msec)
			currentTime = new Date().getTime() - serverTimeDiff;
			//log('Current time: ' + new Date(currentTime));
		
			// Get the time of the next event
			var nextEventTime = getNextEventTime();
			
			// Get time difference
			var timeRemaining = nextEventTime - currentTime;

			// Get a nice string with the time remaining
			var text = formatMsec(timeRemaining);

			//Say "Right now!" if it's going on right now, otherwise format the time remaining
			if (timeRemaining <= 0)
			{
				text = "Right now!";
			}

			//Only update the prompt if we haven't before or if we've crossed into the next event's countdown and prompts have loaded
			if (prompts.length > 0 && activitiesOK)
			{
				var dirty = false;
				if (currentWeek == null)
				{
					currentWeek = true;
					dirty = true;
				}

				if (timeRemaining <= 0)
				{
					if (!currentWeek)
					{
						dirty = true;
					}
					currentWeek = true;
				}
				else if (new Date(currentTime).getUTCDay() < eventDOW)
				{
					if (currentWeek)
					{
						dirty = true;
					}
					currentWeek = false;
				}
				else
				{
					if (!currentWeek)
					{
						dirty = true;
					}
					currentWeek = true;
				}

				if (dirty || forceUpdatePrompt)
				{
					updatePrompt(nextEventTime);
					forceUpdatePrompt = false;
				}
			}
			
			// Update countdown element
			if (countdownElement.innerHTML != text)
			{
				countdownElement.innerHTML = text;
			}

			if (countdownElement2)
			{
				countdownElement2.innerHTML = text;
			}

			//Make a date object to grab the year, month, date and time out of
			var targetTime = new Date(nextEventTime);

			//Build the URL for the timeanddate.com link
			text = "http://www.timeanddate.com/worldclock/fixedtime.html?msg=" + eventTitle + "&iso=" + targetTime.getFullYear();


			//TODO: These probably don't need to be done every tick, but this is the most convenient place to access nextEventTime for the moment

			//FIXME: For some reason that I don't understand, getUTCMonth() is returning the previous month, and has a preceding zero?
			text = text + (targetTime.getUTCMonth() + 1 < 10 ? "0" + (targetTime.getUTCMonth() + 1) : targetTime.getUTCMonth() + 1 );
			
			text = text + (targetTime.getUTCDate() < 10 ? "0" + targetTime.getUTCDate() : targetTime.getUTCDate());
			text = text + "T";
			text = text + (targetTime.getUTCHours() < 10 ? "0" + targetTime.getUTCHours() : targetTime.getUTCHours());
			text = text + (targetTime.getUTCMinutes() < 10 ? "0" + targetTime.getUTCMinutes() : targetTime.getUTCMinutes());
			text = text + "&ah=2";

			document.getElementById("countdownTime").innerHTML = (targetTime.getUTCHours() < 10 ? "0" + targetTime.getUTCHours() : targetTime.getUTCHours()) + ":" + (targetTime.getUTCMinutes() < 10 ? "0" + targetTime.getUTCMinutes() : targetTime.getUTCMinutes());
			document.getElementById("countdownDay").innerHTML = dayArray[targetTime.getUTCDay()]
			
			if (timeanddateElement) {
							
				//Update the timeanddate.com link
				timeanddateElement.href = text;
			}
			
			if (timeanddateElement2) {
							
				//Update the second timeanddate.com link
				timeanddateElement2.href = text;
			}
			
			if (timeElement) {


				text = (targetTime.getUTCHours() <= 12 ? targetTime.getUTCHours() : targetTime.getUTCHours() - 12) + ":";
				text = text + (targetTime.getUTCMinutes() < 10 ? "0" + targetTime.getUTCMinutes() : targetTime.getUTCMinutes());
				
				text = text + (targetTime.getUTCHours() <= 12 ? "am" : "pm") + " UTC";
				
				timeElement.innerHTML = text;
			}
		};

		var updatePrompt = function(t){
			log("Updating prompt");
			var prompt = document.getElementById("eventPrompt");
			var p = document.createElement("span");
			if (prompts.length === 0)
			{
				eventPrompt.innerHTML = "<summary>Warm up prompt (click to reveal):</summary>";
				p.innerHTML = "Error";
				eventPrompt.appendChild(p);
			}
			else
			{
				var origin = new Date(2022,1,12,0, 0, 0, 0);
				origin.setUTCDate(6);
				origin.setUTCHours(0);
				origin.setUTCMinutes(0);
				origin.setUTCSeconds(0);
				origin.setUTCMilliseconds(0);

				var d = new Date(t);
				var day = d.getUTCDay(),
					diff = d.getUTCDate() - day; // adjust when day is sunday
				//log(day);
				//log(d.getUTCDate());
				//log(diff);
				d.setUTCDate(diff);
				//log(d.getUTCDay());
				d.setUTCHours(0);
				d.setUTCMinutes(0);
				d.setUTCSeconds(0);
				d.setUTCMilliseconds(0);

				var weeks = Math.floor(((d.getTime() - origin.getTime())) / (1000 * 60* 60 * 24 * 7)) + 1;
				if (!currentWeek)
				{
					//log("Next week?");
					//weeks = weeks + 1;
				}

				//If we're before the event end time, reset to start of current week. If we're after event end time, reset to start of next week
				if (activityFrequency > 0 && activities.length > 0 && (weeks + activityOffset) % activityFrequency == 0)
				{
					eventPrompt.innerHTML = "<summary>Activity for week " + (weeks - 1) + ":<br />(click to reveal)</summary>";
					var a = activities[Math.floor((activityOffset + weeks) / activityFrequency) % activities.length].split(/\ -\ (.*)/s);
					p.innerHTML = a[0];
					eventPrompt.appendChild(p);
					if (a.length > 1)
					{
						p.innerHTML += "<br /><em>" + a[1] + "</em>";
						p = document.createElement("br");
						eventPrompt.appendChild(p);
					}
				}
				else
				{
					eventPrompt.innerHTML = "<summary>Warm up prompt for week " + (weeks - 1) + ":<br />(click to reveal)</summary>";
					p.innerHTML = prompts[weeks % prompts.length];
					eventPrompt.appendChild(p);
				}
				if (activityFrequency > 0 && activities.length > 0 && (weeks + activityOffset - 1) % activityFrequency == 0)
				{
					var a = activities[Math.floor((activityOffset + weeks - 1) / activityFrequency) % activities.length].split(" - ", 1);
					eventPrompt.appendChild(document.createTextNode(" (previous activity: " + a[0] + ")"));
				}
				else
				{
					eventPrompt.appendChild(document.createTextNode(" (previous: " + prompts[(weeks - 1) % prompts.length] + ")"));
				}
			}
		};

		// Return the tick function as our "external interface"
		return tick;
	})();

	var updateCarousel = function() {
		//console.log("Carousel tick");
		if (document.getElementById("shade").style.display == "block")
		{
			return;
		}

		var left = true;
		var carousels = document.getElementsByClassName("imageContainer");
		Array.prototype.forEach.call(carousels, function(container) {
			Array.prototype.forEach.call(container.children, function(inner) {
				if (left)
				{
					var f = document.createDocumentFragment();
					var image = inner.firstChild;
					inner.lastChild.style.marginLeft = null;
					inner.lastChild.style.marginRight = null;
					f.appendChild(image);
					image.style.marginLeft = null;
					image.style.marginRight = "calc(0.4em + " + (inner.firstChild.offsetWidth) + "px)";
					inner.appendChild(image);
					inner.firstChild.style.marginLeft = "calc(-0.4em - " + (inner.firstChild.offsetWidth) + "px)";
				}
				else
				{
					var f = document.createDocumentFragment();
					var image = inner.lastChild;
					inner.firstChild.style.marginLeft = null;
					inner.firstChild.style.marginRight = null;
					f.appendChild(image);
					image.style.marginRight = null;
					image.style.marginLeft = null;
					inner.insertBefore(image, inner.firstChild);
					inner.firstChild.style.marginLeft = "calc(0.4em + 0.4em + " + (inner.lastChild.offsetWidth) + "px)";
				}
				left = !left;
				return;
			});
		});
	};

	var setupCarousel = function()
	{
		//Remove whitepace text nodes, etc. from image carousels to make scrolling easier to manage
		var carousels = document.getElementsByClassName("imageContainer");
		Array.prototype.forEach.call(carousels, function(container) {
			console.log("Done retrying", container);
			Array.prototype.forEach.call(container.children, function(inner) {
				var child = inner.firstChild;
				while(child != null)
				{
					var newChild = child.nextSibling;
					if (child.nodeType == Node.COMMENT_NODE || child.nodeType == Node.TEXT_NODE)
					{
						child.parentNode.removeChild(child);
					}
					if (child.tagName == "IMG")
					{
						child.style.cursor = "zoom-in";
						child.onclick = function() {
							document.getElementById("previewImage").src = this.src;
							document.getElementById("previewCaption").innerHTML = this.title;
							document.getElementById("shade").style.display = "block";
							document.getElementById("shade").style.opacity = 1;
							document.getElementById("shade").style.backgroundColor = "rgba(0,0,0,0)";
							setTimeout(function() { document.getElementById("shade").style.backgroundColor = "var(--inset-bgcolour)"; }, 1);
							document.getElementById("shadeInner").style.display = "none";
							var scale = this.clientHeight / this.naturalHeight;
							document.getElementById("shadeInner").style.transform = "translate(0, 0) scale(" + scale * 100 + "%)";
							document.getElementById("shadeInner").style.top = "calc(-" + Math.round(0.25 / scale) + "em + "+ (this.getBoundingClientRect().top - this.naturalHeight * 0.5 + this.clientHeight * 0.5) + "px)";
							document.getElementById("shadeInner").style.left = (this.getBoundingClientRect().left - this.naturalWidth * 0.5 + this.clientWidth * 0.5) + "px";
							document.getElementById("shadeInner").style.display = "block";
							setTimeout(function() { 
								document.getElementById("shadeInner").style.transform = "translate(-50%,-50%)";
								document.getElementById("shadeInner").style.top = "50%";
								document.getElementById("shadeInner").style.left = "50%";
								document.getElementById("previewCaption").style.opacity = 1;
								 }, 1);
						};
					}
					child = newChild;
				}
			});
		});
	};

	// Set the timer
	setInterval(updateTimer, 1000);
	setupCarousel();
	updateCarousel();
	setInterval(updateCarousel, 4000);
	
	return true;
};



// Try initializing timer code until it works
var initInterval = null;
var initCount = 0;
var currentWeek = null;

console.log("Retreiving prompt list from ", promptURI);
var request = new XMLHttpRequest();
request.open('GET', promptURI, true);
request.send(null);
request.onreadystatechange = function ()
	{
		if (request.readyState === 4 && request.status === 200)
		{
			var type = request.getResponseHeader('Content-Type');
			if (type.indexOf("text") !== 1)
			{
				prompts = request.responseText.trim().split("\n");
				promptsOK = true;
			}
			else
			{
				log("Error loading prompt list?")
				log(type);
				promptsOK = true;
			}
		}
		else
		{
		}
	}


console.log("Retreiving activity list from ", activityURI);
var request2 = new XMLHttpRequest();
request2.open('GET', activityURI, true);
request2.send(null);
request2.onreadystatechange = function ()
	{
		if (request2.readyState === 4 && request2.status === 200)
		{
			var type = request2.getResponseHeader('Content-Type');
			if (type.indexOf("text") !== 1)
			{
				activities = request2.responseText.trim().split("\n");
				activitiesOK = true;
			}
			else
			{
				log("Error loading activity list?")
				log(type);
				activitiesFrequency = -1;
				activitiesOK = true;
			}
		}
		else
		{
		}
	}


initInterval = setInterval((function() {
	var timerOk = false;
	var handlersOK = false;
	updateEventTime();

	return function() {
		try {
			initCount++;
			
			if (!handlersOK)
			{
				//Try to initialise event handlers for custom schedule and random prompts until they're OK
				if (initEventHandlers())
				{
					handlersOK = true;
					log('Event handlers initialized');
				}
				else
				{
					log('Event handlers failed to initialize');
				}
			}
			
			// Try to initialize timer until it's ok
			if (!timerOk) {
				if (initTimer()) {
					timerOk = true;
					log('Timer initialized');
				} else {
					log('Timer failed to initialize');
				}
			}
			
			// Stop retrying
			if (timerOk && handlersOK) {
				clearInterval(initInterval);
				log('Everything initialized fine after ' + initCount + ' tries, stopping retries!');
			}
			
		} catch (e) {
			; // Discard errors
		}
	};
})(), 100);

})();

